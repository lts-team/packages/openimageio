From: Markus Koschany <apo@debian.org>
Date: Tue, 4 Apr 2023 15:10:32 +0200
Subject: safe_strlen

---
 src/include/OpenImageIO/strutil.h | 18 ++++++++++++++++++
 src/libutil/strutil.cpp           |  8 ++++++++
 2 files changed, 26 insertions(+)

diff --git a/src/include/OpenImageIO/strutil.h b/src/include/OpenImageIO/strutil.h
index 6aa1131..68d25ff 100644
--- a/src/include/OpenImageIO/strutil.h
+++ b/src/include/OpenImageIO/strutil.h
@@ -713,6 +713,24 @@ std::string OIIO_API utf16_to_utf8(const std::wstring& utf16str);
 /// guarantees that there will be a termining 0 character.
 OIIO_API char * safe_strcpy (char *dst, string_view src, size_t size);
 
+/// Return the length of null-terminated string `str`, up to maximum `size`.
+/// If str is nullptr, return 0. This is equivalent to C11 strnlen_s.
+OIIO_API size_t safe_strlen(const char* str, size_t size) noexcept;
+
+
+/// Return a string_view that is a substring of the given C string, ending at
+/// the first null character or after size characters, whichever comes first.
+inline string_view safe_string_view(const char* str, size_t size) noexcept
+{
+    return string_view(str, safe_strlen(str, size));
+}
+
+/// Return a std::string that is a substring of the given C string, ending at
+/// the first null character or after size characters, whichever comes first.
+inline std::string safe_string(const char* str, size_t size)
+{
+    return safe_string_view(str, size);
+}
 
 /// Modify str to trim any whitespace (space, tab, linefeed, cr) from the
 /// front.
diff --git a/src/libutil/strutil.cpp b/src/libutil/strutil.cpp
index a49330a..f3b68e7 100644
--- a/src/libutil/strutil.cpp
+++ b/src/libutil/strutil.cpp
@@ -682,6 +682,14 @@ Strutil::safe_strcpy(char* dst, string_view src, size_t size)
 
 
 
+size_t
+Strutil::safe_strlen(const char* str, size_t size) noexcept
+{
+    return str ? strnlen(str, size) : 0;
+}
+
+
+
 void
 Strutil::skip_whitespace(string_view& str)
 {
